Containerization - 2
-----------------------------------------------------------------------------------
Lab 3: Ứng dụng spring boot
git clone https://github.com/orez-fu/spring
mkdir app_spring && cd spring
./mvnw spring-boot:run
./mvnw clean package
touch Dockerfile
vim Dockerfile

FROM openjdk:8-jdk-alpine
WORKDIR /app
COPY target/demo-0.0.1-SNAPSHOT.jar /app/app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/app.jar"]

Bước 2: Build image
docker build -t java-app:v1 -f Dockerfile .

Bước 3: Run container
docker run -itd --name java-hello -p 10088:8080 java-app:v1

Bước 4: tést
curl localhost:10088

Bước 5: push image to hub
docker tag java-app:v1 hieptm89/java-app:v1
docker login
docker push hieptm89/java-app:v1

-----------------------------------------------------------------------------------
Lab 4: Ứng dụng Angular (Multi stage)
mkdir app_angular_hero && cd app_angular_hero
git clone https://github.com/handuy/angular-hero
Bước 1: Dockerfile
touch Dockerfile
vim Dockerfile

-- stage 1
FROM node:13-alpine as builder 
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app/
RUN npm run build 

--stage 2
FROM nginx:1.17-alpine
COPY --from=builder /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

Bước 2: Tạo image
docker build -t angular-app:v1 -f Dockerfile .

Bước 3: Chạy container
docker run -itd --name angular-app-container -p 8001:80 angular-app:v1

Bước 4: test
curl localhost:8001

-----------------------------------------------------------------------------------
Lab 5: Ứng dụng Python
B1: Dockerfile
mkdir app_python && cd app_python
git clone https://github.com/thinhbui311/docker_flask
touch Dockerfile
vim Dockerfile

FROM python:3.9-slim-buster
WORKDIR /app
COPY requirements.txt /app
RUN pip install -r requirements.txt
COPY . /app/
EXPOSE 5000
ENV FLASK_APP=hello_flash.py
CMD ["flask", "run", "--host", "0.0.0.0"]

B2: Image
docker tag my_flash:0.1 hieptm89/my_flash:0.1
docker build -t my_flash:0.1 -f Dockerfile .

B3: Containerization
docker run -itd -name my_flash_cont -p 5000:5000 my_flash:0.1

B4: test
curl localhost:5000/hello
